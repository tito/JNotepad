package org.jcnc.jnotepad.app.common.constants;

/**
 * SplitPane常量类
 *
 * <p>用于记录SplitPane中子组件的索引</p>
 *
 * @author cccqyu
 */
public class SplitPaneItemConstants {

    /**
     * rootSplitPane中的上部分隔栏索引。
     */
    public static final int ROOT_SPLIT_PANE_TOP_SPLIT_PANE = 0;

    /**
     * rootSplitPane中的底部指令框索引。
     */
    public static final int ROOT_SPLIT_PANE_CMD_BOX = 1;

    /**
     * rootSplitPane中的上部面板的左侧索引。
     */
    public static final int TOP_SPLIT_PANE_DIRECTORY_SIDEBAR_PANE = 0;

    /**
     * rootSplitPane中的上部面板的右侧索引。
     */
    public static final int TOP_SPLIT_PANE_CENTER_TAB_PANE = 1;

}
